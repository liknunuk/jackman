<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{

  // method default
  public function index()
  {
    // membambil dara dari model Model

    // $data['xxx'] = $this->model('Model_xxx')->method();
    // variabel untuk title bar
    $data['title'] = "Home Page";

    $this->view('template/header', $data);
    // $this->view('template/navbar');
    // mengirim $data ke body
    $this->view('home/index', $data);

    // body halaman
    $this->view('template/footer');
  }

  public function addUsulan($npsn = '', $ta = '')
  {
    $data['title']  = "Tambah Usulan";
    $data['npsn']   = $npsn;
    $data['tahun']  = $ta;
    $this->view('template/header', $data);
    $this->view('home/addUsulan', $data);
    $this->view('template/footer');
  }

  public function setUsul()
  {
    if ($_POST['qty'] == 0) {
      if ($this->model('Model_jack')->dellUsulan($_POST['idxLap']) > 0) {
        $_SESSION['pesan'] = 'Data Berhasil di ubah';
        header("Location:" . BASEURL . "Home/addUsulan/" . $_POST['refpage']);
      }
    } else {
      if ($this->model('Model_jack')->ubahUsulan($_POST) > 0) {
        $_SESSION['pesan'] = 'Data Berhasil di ubah';
        header("Location:" . BASEURL . "Home/addUsulan/" . $_POST['refpage']);
      } else {
        echo  "something wrong";
      }
    }
  }


  public function addUsul()
  {
    if ($this->model('Model_jack')->tmbhUsulan($_POST) > 0) {
      $_SESSION['pesan'] = 'Data Berhasil di ubah';
      header("Location:" . BASEURL . "Home/addUsulan/" . $_POST['refpage']);
    } else {
      echo  "something wrong";
    }
  }
}
