<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Cetak extends Controller
{
    public function index(){
        $data['title']='Cetak Laporan';
        $data['kecamatan']=$this->model('Model_public')->kecamatan();
        $this->view('template/header',$data);
        $this->view('cetak/index',$data);
        $this->view('template/footer');
    }

    public function global($tahun,$kecamatan=""){
        $data['title']='Cetak Laporan';
        $data['tahun'] = $tahun;
        $data['kecamatan']=$kecamatan;
        $data['fileName']='LaporanGlobal';
        $this->view('template/header-flat',$data);
        $this->view('cetak/global',$data);
        $this->view('template/footer-flat');
    }

    public function RM($tab,$thn,$nsp,$kec=""){
        $data['src']=$tab;
        $data['thn']=$thn;
        $data['nsp']=$nsp;
        $data['kec']=$kec;
        $data['title']="rekap sarpras";
        $this->view('template/header-flat',$data);
        $this->view('cetak/rm',$data);
    }

    public function AB($tab,$thn,$nsp,$kec=""){
        $data['src']=$tab;
        $data['thn']=$thn;
        $data['nsp']=$nsp;
        $data['kec']=$kec;
        $data['title']="rekap sarpras";
        $this->view('template/header-flat',$data);
        $this->view('cetak/ab',$data);
    }

    public function perkon($tbl,$nsp,$kon,$thn,$kec=''){
        $data['tbl']=$tbl;
        $data['nsp']=$nsp;
        $data['kon']=$kon;
        $data['thn']=$thn;
        $data['kec']=$kec;
        $data['title']='Data Per Kondisi';
        $kcmt = $kec == '' ? 'Kab. Banjarnegara' : 'Kecamatan '.$kec;
        $ket = $kon == 'takpunya' ? 'Tidak Punya' : 'Rusak ' . ucfirst($kon);

        $data['judul'] = "Laporan <span id='nsp'></span>  {$ket} Tahun {$thn} Wilayah {$kcmt}";
        $data['namafile'] = "Data_".$data['nsp']."_".$data['kon']."_".$data['thn']."_".$kcmt;

        $this->view('template/header-flat',$data);
        $this->view('cetak/perkon',$data);
        $this->view('template/footer-flat');

    }


}
