<?php
class Model_jack
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    public function usulanSekolah($data)
    {

        // DATA sekolah;
        $sql = "SELECT nama , kecamatan FROM sekolah WHERE npsn=:npsn";
        $this->db->query($sql);
        $this->db->bind('npsn', $data['npsn']);
        $sekolah = $this->db->resultOne();

        // Data Laporan
        $sql = "SELECT idx_laporan FROM laporan WHERE npsn=:npsn && year(tanggal)=:tahun";
        $this->db->query($sql);
        $this->db->bind('npsn', $data['npsn']);
        $this->db->bind('tahun', $data['tahun']);
        $laporan = $this->db->resultOne();


        // sarpras usulan
        $sql = "SELECT pilvarb , pilteks FROM pilihan WHERE pilgrup = 'usul' ORDER BY pilteks";
        $this->db->query($sql);
        $sarpras = $this->db->resultSet();

        // sarpras diusulkan
        $sql = "SELECT usulan.* FROM usulan , laporan WHERE usulan.idx_lap = laporan.idx_laporan && year(laporan.tanggal)=:tahun && laporan.npsn=:npsn";
        $this->db->query($sql);
        $this->db->bind('npsn', $data['npsn']);
        $this->db->bind('tahun', $data['tahun']);
        $usulan = $this->db->resultSet();

        // binding data, mengamankan karakter aneh
        // $this->db->bind('npsn', $data['npsn']);
        // $this->db->bind('tahun', $data['tahun']);

        //eksekusi query setelah binding data
        // $this->db->execute();

        // pesan baris terpengaruh
        // return $this->db->rowCount();

        // kembalikan hasil pembacaan data, banyak baris;
        return array('sekolah' => $sekolah, 'sarpras' => $sarpras, 'usulan' => $usulan, 'laporan' => $laporan);
    }

    public function ubahUsulan($data)
    {
        $sql = "UPDATE usulan SET banyaknya=:banyak WHERE idx_usulan=:noIndex";
        $this->db->query($sql);
        $this->db->bind('banyak', $data['qty']);
        $this->db->bind('noIndex', $data['idxLap']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function tmbhUsulan($data)
    {
        $sql = "INSERT INTO usulan SET idx_lap=:idx_lap , namaSarpras=:namaSarpras , banyaknya=:banyaknya";
        $this->db->query($sql);
        $this->db->bind('idx_lap', $data['idx_lap']);
        $this->db->bind('namaSarpras', $data['namaSarpras']);
        $this->db->bind('banyaknya', $data['banyaknya']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function dellUsulan($idx)
    {
        $sql = "DELETE FROM usulan WHERE idx_usulan=:idx LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idx', $idx);
        $this->db->execute();
        return $this->db->rowCount();
    }
}
