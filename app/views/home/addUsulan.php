<div class="row">
  <div class="col-md-12 judul-konten">
    <h3>Tambah Usulan</h3>
    <?php
    if (isset($_SESSION['pesan'])) {
      echo '
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
        ' . $_SESSION['pesan'] . '
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      ';
      unset($_SESSION['pesan']);
    }
    ?>
  </div>
</div>
<div class="row">
  <div class="col-sm-3">
    <div class="form-group">
      <label for="usta">Tahun Usulan</label>
      <input type="number" id="usta" class="form-control" value=<?= date('Y'); ?>>
    </div>
  </div>
  <div class="col-sm-8">
    <label for="ussd">Nama Sekolah</label>
    <input type="text" id="ussd" class="form-control">
  </div>
  <div class="col-sm-1">
    <button class="btn btn-primary" id="carusul">Cari Usulan</button>
  </div>
</div>
<?php
//  print_r($data); 
if ($data['npsn'] !== "" && $data['tahun'] !== "") {
  $info = $this->model('Model_jack')->usulanSekolah($data);
  $datausulan = [];
  $keyus = [];
  foreach ($info['usulan'] as $usulan) {
    $namaSarpras = $usulan['namaSarpras'];
    $keyus[$namaSarpras] = [
      'idx_usulan' => $usulan['idx_usulan'],
      'idx_lap' => $usulan['idx_lap'],
      'banyaknya' => $usulan['banyaknya'],
      'keterangan' => $usulan['keterangan'],
      'statusul' => $usulan['statusul'],
      'dimensi' => $usulan['dimensi'],
      'prioritas' => $usulan['prioritas']
    ];
  }

  /*/ 
[idx_usulan] => 23360
[idx_lap] => 0001516
[namaSarpras] => usrkelas
[banyaknya] => 2
[statusul] => Diusulkan
[keterangan] => plafon rusak, usuk mulai rapuh, plesteran rapuh
[dimensi] => 
[prioritas] => 99
/*/
?>
  <div class="row">
    <div class="col-sm-12">
      <table class="table">
        <tbody>
          <tr>
            <td>Nama Sekolah</td>
            <td><?= $info['sekolah']['nama']; ?></td>
          </tr>
          <tr>
            <td>Kecamatan</td>
            <td><?= $info['sekolah']['kecamatan']; ?></td>
          </tr>
          <tr>
            <td>Nomor Laporan</td>
            <td><?= $info['laporan']['idx_laporan']; ?></td>
          </tr>
        </tbody>
      </table>
      <table class="table table-hover table-sm">
        <thead>
          <tr>
            <th>Sarpras Diusulkan</th>
            <th>Banyaknya</th>
            <th>Kontrol</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($info['sarpras'] as $sarpras) :
            $sp = $sarpras['pilvarb'];
          ?>

            <tr>
              <td>
                <?= $sarpras['pilteks']; ?>
              </td>
              <td class="text-right"><?php echo $keyus[$sp]['banyaknya'] == null ? '0' : $keyus[$sp]['banyaknya']; ?></td>
              <td>
                <?php if ($keyus[$sp]['banyaknya'] !== null) : ?>
                  <button class="btn btn-info ganti" id="<?= $info['laporan']['idx_laporan'] . '_' . $sp . '_' . $keyus[$sp]['idx_usulan']; ?>">Update</button>
                <?php else : ?>
                  <button class="btn btn-warning imbuh" id="<?= $info['laporan']['idx_laporan'] . '_' . $sp; ?>">Tambah</button>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php
  }
  // echo "<pre>";
  // print_r($keyus);
  // echo "</pre>";
    ?>
    </div>
  </div> <!-- row 3 -->

  <!-- modal -->
  <div class="modal" tabindex="-1" role="dialog" id="frmGantus">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Ganti Usulan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= BASEURL ?>Home/setUsul" method="post">

            <div class="form-group row">
              <label for="idxLap" class="col-sm-6">No. Index</label>
              <div class="col-sm-6">
                <input type="number" name="idxLap" id="idxLap" class="form-control" readonly>
              </div>
            </div>

            <div class="form-group row">
              <label for="spu" class="col-sm-6">Kode Sarpras</label>
              <div class="col-sm-6">
                <input type="text" name="spu" id="spu" class="form-control" readonly>
              </div>
            </div>

            <div class="form-group row">
              <label for="qty" class="col-sm-6">Banyaknya</label>
              <div class="col-sm-6">
                <input type="number" name="qty" id="qty" class="form-control">
              </div>
            </div>
            <input type="hidden" name="refpage" value="<?= $data['npsn'] . '/' . $data['tahun']; ?>">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="Submit" class="btn btn-primary">Simpan Perubahan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" tabindex="-1" role="dialog" id="frmTambus">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Usulan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= BASEURL ?>Home/addUsul" method="post">

            <div class="form-group row">
              <label for="idxLap" class="col-sm-6">No. Laporan</label>
              <div class="col-sm-6">
                <input type="number" name="idx_lap" id="idx_lap" class="form-control" readonly>
              </div>
            </div>

            <div class="form-group row">
              <label for="spu" class="col-sm-6">Kode Sarpras</label>
              <div class="col-sm-6">
                <input type="text" name="namaSarpras" id="namaSarpras" class="form-control" readonly>
              </div>
            </div>

            <div class="form-group row">
              <label for="qty" class="col-sm-6">Banyaknya</label>
              <div class="col-sm-6">
                <input type="number" name="banyaknya" id="banyaknya" class="form-control" value="1">
              </div>
            </div>
            <input type="hidden" name="refpage" value="<?= $data['npsn'] . '/' . $data['tahun']; ?>">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="Submit" class="btn btn-primary">Simpan Perubahan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->
  <?php $this->view('template/bs4js'); ?>
  <script src="<?= BASEURL; ?>js/jack.js"></script>