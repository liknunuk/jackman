<?php
$tahunIni = date('Y');
$limaTahunLalu = $tahunIni - 5;
?>
<div class="row" style="display:none;">
    <input type="text" id="tahun" value="<?=$data['tahun'];?>">
    <input type="text" id="kecamatan" value="<?=$data['kecamatan'];?>">
</div>
  
<div class="row">
    <div class="col-sm-12">
        <h2>Kondisi Sarpras Sekolah Dasar <span class="labelWilayah"></span> Tahun <span class="labelTahun"></span></h2>
        <a href=# onClick=window.print();>Cetak</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Kondisi Ruang</h3>
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
                <tr>
                    <th>Nama Ruang</th>
                    <th>Kondisi Baik</th>
                    <th>Rusak Ringan</th>
                    <th>Rusak Berat</th>
                    <th>Tidak Punya</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="rlRuang"></tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Kondisi Mebeler</h3>
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
                <tr>
                    <th>Nama Mebel</th>
                    <th>Kondisi Baik</th>
                    <th>Rusak Ringan</th>
                    <th>Rusak Berat</th>
                    <th>Tidak Punya</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="rlMebel"></tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Kondisi Alat Peraga</h3>
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
                <tr>
                    <th>Nama Alper</th>
                    <th>Kondisi Baik</th>
                    <th>Rusak Ringan</th>
                    <th>Rusak Berat</th>
                    <th>Tidak Punya</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="rlAlper"></tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Kondisi Bangunan Pendukung</h3>
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
                <tr>
                    <th>Nama Bangunan</th>
                    <th>Kondisi Baik</th>
                    <th>Rusak Ringan</th>
                    <th>Rusak Berat</th>
                    <th>Tidak Punya</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="rlBgduk"></tbody>
          </table>
        </div>
    </div>
</div>


<?php $this->view('template/bs4js'); ?>
<script src="<?=BASEURL;?>js/cetak-global.js"></script>