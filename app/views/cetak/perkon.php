<div class="row">
    <div class="col-sm-6">
        <h4 id="judul"><?=$data['judul'];?></h4>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
    <a href="<?=BASEURL;?>Cetak">Kembali</a>
        <table class="table table-sm">
            <thead>
                <tr>
                    <th>Sekolah</th>
                    <th>Kecamatan</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody id="datasp"></tbody>
        </table>
    </div>
</div>

<?php 
if($data['tbl'] == 'v_lpr' || $data['tbl'] == 'v_lpm'){
    $meth = 'perkonRM';
}else{
    $meth = 'perkonAB';
}
$this->view('template/bs4js'); 
?>
<script type="text/javascript" src="//unpkg.com/xlsx/dist/xlsx.full.min.js"></script>

<script>
$(document).ready( function(){
    let jumlah =0;
    $.getJSON( datasource + `Statistik/<?=$meth;?>/<?=$data['tbl'];?>/<?=$data['nsp'];?>/<?=$data['kon'];?>/<?=$data['thn'];?>/<?=$data['kec'];?>` , function (sekolah){
        exportToSpreadSheet(sekolah.data,"<?=$data['namafile'];?>");
        $('#datasp tr').remove();
        $('#nsp').text(sekolah.sp);
        $.each(sekolah.data , function(i,data){
            jumlah+=parseInt(data.jumlah);
            $('#datasp').append(`
            <tr>
            <td>${data.sekolah}</td>
            <td>${data.kecamatan}</td>
            <td class='text-right'>${data.jumlah}</td>
            </tr>
            `);
        })
        $('#datasp').append(`
        <tr class='text-right'>
        <td colspan='2'>Jumlah Total</td>
        <th>${jumlah}</th>
        </tr>
        `);
    })
});
</script>

<script>
function exportToSpreadSheet(data,namaFile){
    var createXLSLFormatObj = [];
 
        /* XLS Head Columns */
        var xlsHeader = ["Sekolah" , "Kecamatan" , "Jumlah"];
 
        /* XLS Rows Data */
        var xlsRows = data;
 
        createXLSLFormatObj.push(xlsHeader);
        $.each(xlsRows, function(index, value) {
            var innerRowData = [];
            $("tbody").append(`
            <tr>
            <td>${value.sekolah}</td>
            <td>${value.kecamatan}</td>
            <td>${value.jumlah}</td>
            </tr>`);
            $.each(value, function(ind, val) {
                innerRowData.push(val);
            });
            createXLSLFormatObj.push(innerRowData);
        });
 
 
        /* File Name */
        var filename = namaFile+'.xlsx';
 
        /* Sheet Name */
        var ws_name = "Sheet1";
 
        if (typeof console !== 'undefined') console.log(new Date());
        var wb = XLSX.utils.book_new(),
            ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);
 
        /* Add worksheet to workbook */
        XLSX.utils.book_append_sheet(wb, ws, ws_name);
 
        /* Write workbook and Download */
        if (typeof console !== 'undefined') console.log(new Date());
        XLSX.writeFile(wb, filename);
        if (typeof console !== 'undefined') console.log(new Date());
}
</script>