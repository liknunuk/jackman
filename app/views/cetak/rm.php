<?php
if($data['kec']==''){
    $wil = 'Kabupaten Banjarnegara';
}else{
    $wil = 'Kecamatan '.$data['kec'];
}
?>
<h4><span id="sp"></span> TAHUN <?=$data['thn'].' '.strtoupper($wil);?> </h4>
<table class="table table-sm">
    <thead>
        <tr>
            <th>No.</th>
            <th>SD</th>
            <th>Kecamatan</th>
            <th>Baik</th>
            <th>Ringan</th>
            <th>Sedang/Berat</th>
        </tr>
    </thead>
    <tbody id="rekapsp"></tbody>
</table>
<?php
$this->view('template/bs4js');

//Laporan/sdPelaporRM/v_lpr/2019/rperpus/
?>
<script>
$(document).ready(function(){
    let gbaik=0,gringan=0,gberat=0;
    $.getJSON( datasource + `Laporan/sdPelaporRM/<?=$data['src'];?>/<?=$data['thn'];?>/<?=$data['nsp'];?>/<?=$data['kec'];?>` , function(rekap){
        console.log(rekap);
        $('#sp').text('');
        $('#sp').text(rekap.sarpras);
        let urut=1;
        $('#rekapsp tr').remove();
        $.each(rekap.data, function(i,data){
            gbaik+=parseInt(data.baik);
            gringan+=parseInt(data.sedang);
            gberat+=parseInt(data.parah);
            $('#rekapsp').append(`
            <tr>
            <td class='text-right'>${urut}.</td>
            <td>${data.nama}</td>
            <td>${data.kecamatan}</td>
            <td class='text-right' width='125'>${data.baik}</td>
            <td class='text-right' width='125'>${data.sedang}</td>
            <td class='text-right' width='125'>${data.parah}</td>
            </tr>
            `);
            urut+=1;
        })
        $('#rekapsp').append(`
            <tr>
            <td colspan='3'>Jumlah Keseluruhan</td>
            <td class='text-right'>${gbaik}</td>
            <td class='text-right'>${gringan}</td>
            <td class='text-right'>${gberat}</td>
            </tr>
        `)
    })
})
</script>