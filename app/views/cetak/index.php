<?php
$tahunIni = date('Y');
$limaTahunLalu = $tahunIni - 5;
?>
<div class="row">
  <div class="col-sm-3">
    <label for="tahun">Tahun</label><br>
    <select id="tahun" class="form-control">
      <option value="">Pilih Tahun</option>
      <?php for($i=$tahunIni;$i >= $limaTahunLalu; $i--): ?>
      <option value="<?=$i;?>">Tahun <?=$i;?></option>
      <?php endfor; ?>
    </select>
  </div>
  <div class="col-sm-3">
  <label for="kecamatan">Kecamatan</label>
  <select id="kecamatan" class="form-control">
  <option value="">SEMUA KECAMATAN</option>
  <?php foreach($data['kecamatan'] AS $kecamatan):?>
    <option value="<?=$kecamatan['pilvarb'];?>"><?=$kecamatan['pilteks'];?></option>
  <?php endforeach; ?>
  </select>
  </div>
  <div class="col-sm-2">
    <br>
    <button class="btn btn-primary" id="showReports">Buka Laporan</button>
  </div>
  <div class="col-sm-2">
    <br>
    <button class="btn btn-primary" id="printReports">Cetak Laporan</button>
  </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <h2>Kondisi Sarpras Sekolah Dasar <span class="labelWilayah"></span> Tahun <span class="labelTahun"></span></h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Kondisi Ruang</h3>
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
                <tr>
                    <th>Nama Ruang</th>
                    <th>Kondisi Baik</th>
                    <th>Rusak Ringan</th>
                    <th>Rusak Berat</th>
                    <th>Tidak Punya</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="rlRuang"></tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Kondisi Mebeler</h3>
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
                <tr>
                    <th>Nama Mebel</th>
                    <th>Kondisi Baik</th>
                    <th>Rusak Ringan</th>
                    <th>Rusak Berat</th>
                    <th>Tidak Punya</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="rlMebel"></tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Kondisi Alat Peraga</h3>
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
                <tr>
                    <th>Nama Alper</th>
                    <th>Kondisi Baik</th>
                    <th>Rusak Ringan</th>
                    <th>Rusak Berat</th>
                    <th>Tidak Punya</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="rlAlper"></tbody>
          </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Kondisi Bangunan Pendukung</h3>
        <div class="table-responsive">
          <table class="table table-sm">
            <thead>
                <tr>
                    <th>Nama Bangunan</th>
                    <th>Kondisi Baik</th>
                    <th>Rusak Ringan</th>
                    <th>Rusak Berat</th>
                    <th>Tidak Punya</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody id="rlBgduk"></tbody>
          </table>
        </div>
    </div>
</div>


<?php $this->view('template/bs4js'); ?>
<script src="<?=BASEURL;?>js/cetak.js"></script>