<?php
if($data['kec']==''){
    $wil = 'Kabupaten Banjarnegara';
}else{
    $wil = 'Kecamatan '.$data['kec'];
}
?>
<h4><span id="sp"></span> TAHUN <?=$data['thn'].' '.strtoupper($wil);?> </h4>
<table class="table table-sm">
    <thead>
        <tr>
            <th>No.</th>
            <th>SD</th>
            <th>Kecamatan</th>
            <th>Kondisi</th>
        </tr>
    </thead>
    <tbody id="rekapsp"></tbody>
</table>
<?php
$this->view('template/bs4js');

//Laporan/sdPelaporRM/v_lpr/2019/rperpus/
?>
<script>
$(document).ready(function(){
    
    $.getJSON( datasource + `Laporan/sdPelaporAB/<?=$data['src'];?>/<?=$data['thn'];?>/<?=$data['nsp'];?>/<?=$data['kec'];?>` , function(rekap){
        console.log(rekap);
        $('#sp').text('');
        $('#sp').text(rekap.sarpras);
        let urut=1;
        $('#rekapsp tr').remove();
        $.each(rekap.data, function(i,data){
            
            $('#rekapsp').append(`
            <tr>
            <td class='text-right'>${urut}.</td>
            <td>${data.nama}</td>
            <td>${data.kecamatan}</td>
            <td>${data.kondisi}</td>
            </tr>
            `);
            urut+=1;
        })
        
    })
})
</script>