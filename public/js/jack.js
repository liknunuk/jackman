const datasources = "https://simudik-sd.dindikpora.banjarnegarakab.go.id/Datapool/";
const homebaseurl = "https://sijackman.dindikpora.banjarnegarakab.go.id/";

// const datasources = 'https://likgroho.mugeno.org/simudik/public/Datapool/';
// const homebaseurl = 'https://likgroho.mugeno.org/jackman/public/';

$(document).ready( function(){
    $("#ussd").keyup( function(){
        let sekolah = $(this).val();
        if( sekolah.length >=4 ){
            let datasekolah = [];
            $.getJSON( datasources + `getSekolah/${sekolah}`  , function (skl){
                $.each( skl, function(i,data){
                    datasekolah.push(data.npsn+'-'+data.nama)
                })
            })
            $("#ussd").autocomplete({ source: datasekolah})
        }
    })

    $("#carusul").click( function(){
        let ds = $("#ussd").val().split('-');
        let npsn = ds[0];
        let ta = $("#usta").val();
        window.location = homebaseurl + `Home/addUsulan/${npsn}/${ta}`;
    })

    $(".ganti").click( function(){
        let par = this.id.split("_"), idx, spu, qty;
        idx = par[2];
        spu = par[1];
        qty = parseInt($(this).parent('td').parent('tr').children('td:nth-child(2)').text());
        ubahUsulan(idx,spu,qty);
    })

    $(".imbuh").click(function(){
        let par,idx_lap,namaSarpras;
        par = this.id.split('_');
        idx_lap = par[0];
        namaSarpras = par[1];
        $("#frmTambus").modal('show');
        $('#idx_lap').val(idx_lap);
        $('#namaSarpras').val(namaSarpras);
    })
})


function ubahUsulan(idx,spu,qty){
    $("#frmGantus").modal('show');
    $("#idxLap").val(idx);
    $("#spu").val(spu);
    $("#qty").val(qty);
}
