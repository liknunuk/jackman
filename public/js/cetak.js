$(document).ready( function(){
    $('#showReports').click( function(){
        let tahun = $('#tahun').val();
        let camat = $('#kecamatan').val();
        let wilayah='';
        $('.labelTahun').html(tahun);
        if(camat == ''){
            wilayah = 'Kabupaten Banjarnegara';
        }else{
            wilayah = 'Kecamatan '+camat;
        }
        $('.labelWilayah').html(wilayah);
        getLaporanGlobal(tahun,camat);
    });
    
    $("#printReports").click(function(){
        let tahun = $('#tahun').val();
        let camat = $('#kecamatan').val();
        window.location.href=urlservice+"Cetak/global/"+tahun+"/"+camat;
    });
});

function getLaporanGlobal(tahun,kecamatan = ''){

    $.getJSON( datasource + `Laporan/rekapGlobal/${tahun}/${kecamatan}` , function(reports){
    console.log(reports);
    $.each(reports, function(i,data){
        // laporan ruang
        $('#rlRuang tr').remove();
        $.each(reports.ruang, function(i,rg){
            let total = parseInt(rg.baik) + parseInt(rg.ringan) + parseInt(rg.berat);
            let namaRuang = rg.ruang.replace('KONDISI ','');
            $('#rlRuang').append(`
            <tr>
            <td><a href=javascript:void(0) onclick=sdPelaporRM('v_lpr','${rg.namaRuang}',${tahun},'${kecamatan}')>${namaRuang}</a></td>
            <td align='right'>
            <a href='`+urlservice+`cetak/perkon/v_lpr/${rg.namaRuang}/baik/${tahun}/${kecamatan}'>
            ${parseInt(rg.baik).toLocaleString('id-ID')}
            </a>
            </td>
            <td align='right'>
            <a href='`+urlservice+`cetak/perkon/v_lpr/${rg.namaRuang}/ringan/${tahun}/${kecamatan}'>
            ${parseInt(rg.ringan).toLocaleString('id-ID')}</a></td>
            <td align='right'>
            <a href='`+urlservice+`cetak/perkon/v_lpr/${rg.namaRuang}/berat/${tahun}/${kecamatan}'>
            ${parseInt(rg.berat).toLocaleString('id-ID')}</a></td>
            <td align='right'>
            <a href='`+urlservice+`cetak/perkon/v_lpr/${rg.namaRuang}/takpunya/${tahun}/${kecamatan}'>
            ${parseInt(rg.takPunya).toLocaleString('id-ID')}</td>
            <td align='right'>${parseInt(total).toLocaleString('id-ID')}</td>
            </tr>
            `);
        });
        
        // laporan mebel
        $('#rlMebel tr').remove();
        //console.log(reports.mebel);
        $.each(reports.mebel, function(i,sp){
            let total = parseInt(sp.baik) + parseInt(sp.ringan) + parseInt(sp.berat);
            let namaMebel = sp.mebel.replace('KONDISI ','');
            $('#rlMebel').append(`
            <tr>
            <td><a href=javascript:void(0) onclick=sdPelaporRM('v_lpm','${sp.namaMebel}',${tahun},'${kecamatan}')>${namaMebel}</a></td>
            <td align='right'>
            <a href='`+urlservice+`cetak/perkon/v_lpm/${sp.namaMebel}/baik/${tahun}/${kecamatan}'>
            ${parseInt(sp.baik).toLocaleString('id-ID')}</a></td>
            <td align='right'>
            <a href='`+urlservice+`cetak/perkon/v_lpm/${sp.namaMebel}/ringan/${tahun}/${kecamatan}'>
            ${parseInt(sp.ringan).toLocaleString('id-ID')}</a></td>
            <td align='right'>
            <a href='`+urlservice+`cetak/perkon/v_lpm/${sp.namaMebel}/berat/${tahun}/${kecamatan}'>
            ${parseInt(sp.berat).toLocaleString('id-ID')}</a></td>
            <td align='right'>
            <a href='`+urlservice+`cetak/perkon/v_lpm/${sp.namaMebel}/takpunya/${tahun}/${kecamatan}'>
            ${parseInt(sp.takPunya).toLocaleString('id-ID')}</a></td>
            <td align='right'>${parseInt(total).toLocaleString('id-ID')}</td>
            </tr>
            `);
        });
        
        
            // laporan alper
            $('#rlAlper tr').remove();
            $.each(reports.alper, function(i,sp){
                let total = parseInt(sp.baik) + parseInt(sp.ringan) + parseInt(sp.berat);
                let namaAlper = sp.sarpras.replace('KONDISI ','');
                
                $('#rlAlper').append(`
                <tr>
                <td><a href=javascript:void(0) onclick=sdPelaporAB('v_lpa','${sp.namaSarpras}',${tahun},'${kecamatan}')>${namaAlper}</a></td>
                <td align='right'>
                <a href='`+urlservice+`cetak/perkon/v_lpa/${sp.namaSarpras}/baik/${tahun}/${kecamatan}'>
                ${parseInt(sp.baik).toLocaleString('id-ID')}</a></td>
                <td align='right'>
                <a href='`+urlservice+`cetak/perkon/v_lpa/${sp.namaSarpras}/ringan/${tahun}/${kecamatan}'>
                ${parseInt(sp.ringan).toLocaleString('id-ID')}</a></td>
                <td align='right'>
                <a href='`+urlservice+`cetak/perkon/v_lpa/${sp.namaSarpras}/berat/${tahun}/${kecamatan}'>
                ${parseInt(sp.berat).toLocaleString('id-ID')}</a></td>
                <td align='right'>
                <a href='`+urlservice+`cetak/perkon/v_lpa/${sp.namaSarpras}/takpunya/${tahun}/${kecamatan}'>
                ${parseInt(sp.tidakAda).toLocaleString('id-ID')}</a></td>
                <td align='right'>${parseInt(total).toLocaleString('id-ID')}</td>
                </tr>
                `);
            });

            // laporan bangunan
            $('#rlBgduk tr').remove();
            $.each(reports.bgduk, function(i,sp){
                let total = parseInt(sp.baik) + parseInt(sp.ringan) + parseInt(sp.berat);
                let namaAlper = sp.sarpras.replace('KONDISI ','');
                
                $('#rlBgduk').append(`
                <tr>
                <td><a href=javascript:void(0) onclick=sdPelaporAB('v_lpb','${sp.namaSarpras}',${tahun},'${kecamatan}')>${namaAlper}</a></td>
                <td align='right'>
                <a href='`+urlservice+`cetak/perkon/v_lpb/${sp.namaSarpras}/baik/${tahun}/${kecamatan}'>
                ${parseInt(sp.baik).toLocaleString('id-ID')}</a></td>
                <td align='right'>
                <a href='`+urlservice+`cetak/perkon/v_lpb/${sp.namaSarpras}/ringan/${tahun}/${kecamatan}'>
                ${parseInt(sp.ringan).toLocaleString('id-ID')}</a></td>
                <td align='right'>
                <a href='`+urlservice+`cetak/perkon/v_lpb/${sp.namaSarpras}/berat/${tahun}/${kecamatan}'>
                ${parseInt(sp.berat).toLocaleString('id-ID')}</a></td>
                <td align='right'>
                <a href='`+urlservice+`cetak/perkon/v_lpb/${sp.namaSarpras}/takpunya/${tahun}/${kecamatan}'>
                ${parseInt(sp.tidakAda).toLocaleString('id-ID')}</a></td>
                <td align='right'>${parseInt(total).toLocaleString('id-ID')}</td>
                </tr>
                `);
            });
        });
    });
}

function sdPelaporRM(tbl,osp,ta,kec=''){
    window.location.href=urlservice+`Cetak/RM/${tbl}/${ta}/${osp}/${kec}`;
}

function sdPelaporAB(tbl,osp,ta,kec=''){
    
    window.location.href=urlservice+`Cetak/AB/${tbl}/${ta}/${osp}/${kec}`;
}